#ifndef LIBLINUX_H
#define LIBLINUX_H

#include <liblinux/syscall.h>
#include <stddef.h>
#include <sys/types.h>

struct FILE {
  char *filename;
  int fd;
};
typedef struct FILE FILE;

extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;

#define stdin stdin
#define stdout stdout
#define stderr stderr

_Noreturn void sys_exit(int status);
int sys_open(const char *pathname, int flags);
int sys_close(int fd);
ssize_t sys_write(int fd, const void *buf, size_t count); 
ssize_t sys_read(int fd, void *buf, size_t count);
void *sys_mmap(void *addr, size_t len, int prot, int flags, int fildes, off_t off);
int sys_munmap(void *addr, size_t len);
int sys_rename(const char *old, const char *new);
ssize_t sys_getrandom(void *buf, size_t buflen, unsigned int flags);

// TODO

#endif
