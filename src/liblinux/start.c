#include <liblinux/syscall.h>
#include <liblinux/start.h>

void _exit(int status) {
  __asm("mov %eax, %edi\n");
  syscall(__NR_exit, status);
}

void _start() {
  __asm("xor %ebp, %ebp\n"
	"mov (%rsp), %edi\n"
	"lea 8(%rsp), %rsi\n"
	"lea 16(%rsp,%rdi,8), %rdx\n"
	"xor %eax, %eax\n");
  _exit(main());
}
