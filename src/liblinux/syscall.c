long syscall(long syscall, long _1, long _2, long _3, long _4, long _5, long _6) {
  register long rax __asm__("rax") = syscall;
  register long rdi __asm__("rdi") = _1;
  register long rsi __asm__("rsi") = _2;
  register long rdx __asm__("rdx") = _3;
  register long r10 __asm__("r10") = _4;
  register long r8  __asm__("r8")  = _5;
  register long r9  __asm__("r9")  = _6;
  
  __asm__ volatile (
		    "syscall"
		    : "+r" (rax),
		      "+r" (r8), "+r" (r9), "+r" (r10)
		    : "r" (rdi), "r" (rsi), "r" (rdx)
		    : "rcx", "r11", "cc", "memory");
  return rax;
}
