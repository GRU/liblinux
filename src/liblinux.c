#include <liblinux.h>
#include <liblinux/syscall.h>

FILE __stdin_FILE = {
  .fd = 0
};

FILE *stdin = &__stdin_FILE;

FILE __stdout_FILE = {
  .fd = 1
};

FILE *stdout = &__stdout_FILE;

FILE __stderr_FILE = {
  .fd = 2
};

FILE *stderr = &__stderr_FILE;

_Noreturn void sys_exit(int status) {
  syscall(__NR_exit, status);
}

int sys_open(const char *pathname, int flags) {
  return syscall(__NR_open, pathname, flags);
}

int sys_close(int fd) {
  return syscall(__NR_close, fd);
}

ssize_t sys_write(int fd, const void *buf, size_t count) {
  return syscall(__NR_write, fd, buf, count);
}

ssize_t sys_read(int fd, void *buf, size_t count) {
  return syscall(__NR_read, fd, buf, count);
}

void *sys_mmap(void *addr, size_t len, int prot, int flags, int fildes, off_t off) {
  return (void*)syscall(__NR_mmap, addr, len, prot, flags, fildes, off);
}

int sys_munmap(void *addr, size_t len) {
  return syscall(__NR_munmap, addr, len);
}

int sys_rename(const char *old, const char *new) {
  return syscall(__NR_rename, old, new);
}

ssize_t sys_getrandom(void *buf, size_t buflen, unsigned int flags) {
  return syscall(__NR_getrandom, buf, buflen, flags);
}
